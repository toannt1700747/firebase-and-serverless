import db from '../../config/db/db';
import pick from '../../helpers/pick';

const collection = db.collection('todos');

export async function getList({ limit = 10, sort = 'desc' } = {}) {
  let queryTodo = collection;
  if (sort) {
    queryTodo = queryTodo.orderBy('createdAt', sort);
  }
  if (limit) {
    queryTodo = queryTodo.limit(parseInt(limit));
  }

  const { docs } = await queryTodo.get();
  return docs.map((doc) => {
    return {
      id: doc.id,
      ...doc.data(),
    };
  });
}

export async function getOne({ id, fields }) {
  const doc = await collection.doc(id).get();
  if (!doc.data()) throw new Error('Todo not found !!!');
  const todo = {
    id: doc.id,
    ...doc.data(),
  };
  if (fields) {
    fields = fields.split(',');
    return pick(todo, fields);
  }
  return todo;
}

export async function add(data = {}) {
  const addTodo = {
    ...data,
    complete: false,
    createdAt: new Date(),
  };
  const todo = await collection.add(addTodo);
  return { id: todo.id, ...addTodo };
}

export async function update(id) {
  return collection.doc(id).update({ complete: true });
}

export async function updateMass(indexArr = []) {
  const batch = db.batch();
  indexArr.forEach((id) => {
    batch.update(collection.doc(id), { complete: true });
  });

  return batch.commit();
}

export async function remove(id) {
  return collection.doc(id).delete();
}

export async function removeMass(indexArr = []) {
  const batch = db.batch();
  indexArr.forEach((id) => {
    batch.delete(collection.doc(id));
  });
  return batch.commit();
}
