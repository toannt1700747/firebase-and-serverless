const yup = require('yup');

export async function todoInputMiddleware(ctx, next) {
  try {
    const postData = ctx.request.body;
    let schema = yup.object().shape({
      title: yup.string().required(),
    });

    await schema.validate(postData);

    return next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
}

export async function todoFieldMiddleware(ctx, next) {
  try {
    const fieldQuery = ctx.query.fields;

    if (fieldQuery) {
      let fields = fieldQuery.split(',');
      let schema = yup.array().of(yup.mixed().oneOf(['title', 'complete']));
      await schema.validate(fields);
    }

    return next();
  } catch (e) {
    ctx.status = 400;
    ctx.body = {
      success: false,
      errors: e.errors,
      errorName: e.name,
    };
  }
}
